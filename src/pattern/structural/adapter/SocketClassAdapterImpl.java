package pattern.structural.adapter;

public class SocketClassAdapterImpl implements SocketAdapter {

    private Socket socket = new Socket();

    @Override
    public Volt get120Volt() {
        return socket.getVolts();
    }

    @Override
    public Volt get12Volt() {
        return convertVolt(socket.getVolts(), 10);
    }

    @Override
    public Volt get3Volt() {
        return convertVolt(socket.getVolts(), 40);
    }

    public Volt convertVolt(Volt volt, int i) {
        return new Volt(volt.getVolts()/i);
    }
}
