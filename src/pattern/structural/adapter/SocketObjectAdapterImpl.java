package pattern.structural.adapter;

public class SocketObjectAdapterImpl extends Socket implements SocketAdapter {

    @Override
    public Volt get120Volt() {
        return getVolts();
    }

    @Override
    public Volt get12Volt() {
        return convertVolt(getVolts(), 10);
    }

    @Override
    public Volt get3Volt() {
        return convertVolt(getVolts(), 40);
    }

    public Volt convertVolt(Volt volt, int i) {
        return new Volt(volt.getVolts()/i);
    }

}
