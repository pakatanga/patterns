package pattern.structural.proxy;

public interface CommandExecutor {

    void runCommand(String command) throws Exception;

}
