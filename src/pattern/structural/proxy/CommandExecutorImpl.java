package pattern.structural.proxy;

import java.io.IOException;

public class CommandExecutorImpl implements CommandExecutor {

    CommandExecutorImpl() {

    }

    @Override
    public void runCommand(String command) throws IOException {
        Runtime.getRuntime().exec(command);
    }
}
