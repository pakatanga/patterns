package pattern.structural.proxy;

public class CommandExecutorProxy implements CommandExecutor {

    private boolean isAdmin;
    private CommandExecutor executor;

    public CommandExecutorProxy(String user, String pwd){
        if("PAACOOOO".equals(user) && "supersegura".equals(pwd)) {
            this.isAdmin = true;
        }
        this.executor = new CommandExecutorImpl();
    }

    @Override
    public void runCommand(String command) throws Exception {
        if (this.isAdmin) {
            executor.runCommand(command);
        } else {
            if (command.trim().toLowerCase().startsWith("rm")) {
                throw new Exception("NOOOOOO, eso así no es");
            } else {
                executor.runCommand(command);
            }
        }
    }
}
