package pattern.structural.bridge;

public class Triangle extends Shape {

    public Triangle(Color color) {
        super(color);
    }

    @Override
    public void paint() {
        color.applyColor();
    }
}
