package pattern.structural.bridge;

public class Pentagon extends Shape {

    public Pentagon(Color color) {
        super(color);
    }

    @Override
    public void paint() {
        color.applyColor();
    }
}
