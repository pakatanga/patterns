package pattern.structural.decorator;

public interface Car {

    void assemble();

}
