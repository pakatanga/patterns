package pattern.structural.composite;

public interface Shape {

    void draw(String fillColor);

}
