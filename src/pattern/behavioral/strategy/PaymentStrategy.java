package pattern.behavioral.strategy;

public interface PaymentStrategy {

  void pay(int value);

}
