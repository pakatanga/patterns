package pattern.behavioral.strategy;

public class PaypalPayment implements PaymentStrategy {

  private String user;
  private String pwd;

  public PaypalPayment(String user, String pwd) {
    this.user = user;
    this.pwd = pwd;
  }

  @Override
  public void pay(int value) {
    System.out.println("Paying with paypal: " + value);
  }
}
