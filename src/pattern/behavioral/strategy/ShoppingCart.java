package pattern.behavioral.strategy;

import java.util.List;

public class ShoppingCart {

  List<Item> items;

  public ShoppingCart() {

  }

  public void addItem(Item item) {

  }

  public void removeItem(Item item) {

  }

  private int totalCost() {
    int total = 0;
    for (Item item : items) {
      total += item.getAmount();
    }
    return total;
  }

  public void pay(PaymentStrategy paymentStrategy) {
    paymentStrategy.pay(totalCost());
  }
}
