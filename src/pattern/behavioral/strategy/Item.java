package pattern.behavioral.strategy;

public class Item {

  private String upcCode;
  private int amount;

  public Item(String upcCode, int amount) {
    this.upcCode = upcCode;
    this.amount = amount;
  }

  public String getUpcCode() {
    return upcCode;
  }

  public int getAmount() {
    return amount;
  }
}
