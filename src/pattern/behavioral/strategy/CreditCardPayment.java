package pattern.behavioral.strategy;

public class CreditCardPayment implements PaymentStrategy {

    private String name;
    private String cardNumber;
    private String cvv;

    public CreditCardPayment(String name, String cardNumber, String cvv) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.cvv = cvv;
    }

    @Override public void pay(int value) {
        System.out.println("Paying with Credit card: " + value);
    }
}
