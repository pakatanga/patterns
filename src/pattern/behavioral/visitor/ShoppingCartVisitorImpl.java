package pattern.behavioral.visitor;

public class ShoppingCartVisitorImpl implements ShoppingCartVisitor {

  @Override
  public int visit(Book book) {
    int cost = 0;
    int price = book.getPrice();
    if (price < 50) {
      return price - 5;
    }
    return price;

  }

  @Override
  public int visit(Fruit fruit) {
    return fruit.getPricePerKg() * fruit.getWeight();
  }
}
