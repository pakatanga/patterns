package pattern.behavioral.visitor;

public interface ItemElement {

  int calculatePrice(ShoppingCartVisitor shoppingCartVisitor);

}
