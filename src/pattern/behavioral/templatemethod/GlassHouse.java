package pattern.behavioral.templatemethod;

public class GlassHouse extends HouseTemplate {

    @Override public void buildWalls() {
        System.out.println("building glass walls");
    }

    @Override public void buildPillars() {
        System.out.println("building glass pillars");
    }
}
