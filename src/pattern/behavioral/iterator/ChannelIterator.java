package pattern.behavioral.iterator;

public interface ChannelIterator {

  boolean hasNext();

  Channel next();

}
