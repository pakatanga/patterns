package pattern.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class ChannelCollectionImpl implements ChannelCollection {

  private List<Channel> channelList;

  public ChannelCollectionImpl() {
    channelList = new ArrayList<>();
  }

  @Override
  public void addChannel(Channel channel) {
    this.channelList.add(channel);
  }

  @Override
  public void removeChannel(Channel channel) {
    this.channelList.remove(channel);
  }

  @Override
  public ChannelIterator iterator(ChannelTypeEnum type) {
    return new ChannelIteratorImpl(type, this.channelList);
  }

  private class ChannelIteratorImpl implements ChannelIterator {

    private ChannelTypeEnum type;
    private List<Channel> channelList;
    private int position;

    public ChannelIteratorImpl(ChannelTypeEnum type, List<Channel> channelList) {
      this.type = type;
      this.channelList = channelList;
    }

    @Override
    public boolean hasNext() {
      while (position < channelList.size()) {
        Channel c = channelList.get(position);
        if (c.getType().equals(type) || type.equals(ChannelTypeEnum.ALL)) {
          return true;
        } else
          position++;
      }
      return false;
    }

    @Override
    public Channel next() {
      Channel channel = channelList.get(position++);
      return channel;
    }
  }
}
