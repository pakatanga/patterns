package pattern.behavioral.action;

public class StartedTVState implements State {

  @Override
  public void doAction() {
    System.out.println("Stoping TV");
  }

}
