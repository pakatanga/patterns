package pattern.behavioral.action;

public class StoppedTVAction implements State {

  @Override
  public void doAction() {
    System.out.println("Starting TV");
  }

}
