package pattern.behavioral.action;

public interface State {

  void doAction();

}
