package pattern.behavioral.chainofresponsability;

public abstract class CurrencyDispenser implements DispenseChain {

  private int amount;

  private DispenseChain chain;

  public CurrencyDispenser(int amount) {
    this.amount = amount;
  }

  @Override
  public void setNextChain(DispenseChain nextChain) {
    this.chain = nextChain;
  }

  @Override
  public void dispense(Currency currency) {
    if (currency.getAmount() >= amount) {
      int numOfCurrencies = currency.getAmount() / amount;
      int remainder = currency.getAmount() % amount;
      System.out.println("Dispensing " + numOfCurrencies + " of " + amount);
      this.chain.dispense(new Currency(remainder));
    } else {
      this.chain.dispense(currency);
    }
  }
}
