package pattern.behavioral.chainofresponsability;

public interface Dispense {

  void dispense(Currency currency);

}
