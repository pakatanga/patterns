package pattern.behavioral.chainofresponsability;

public class Currency50Dispenser extends CurrencyDispenser {

  public Currency50Dispenser() {
    super(50);
  }

}
