package pattern.behavioral.chainofresponsability;

public class Currency20Dispenser extends CurrencyDispenser {

  public Currency20Dispenser() {
    super(20);
  }

}
