package pattern.behavioral.chainofresponsability;

public class Currency10Dispenser extends CurrencyDispenser {

  public Currency10Dispenser() {
    super(10);
  }

}
