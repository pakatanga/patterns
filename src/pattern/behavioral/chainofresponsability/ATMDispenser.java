package pattern.behavioral.chainofresponsability;

public class ATMDispenser implements Dispense {

  private DispenseChain dispenseChain;

  public ATMDispenser() {
    this.dispenseChain = new Currency50Dispenser();
    DispenseChain currency20Dispenser = new Currency20Dispenser();
    DispenseChain currency10Dispenser = new Currency10Dispenser();

    currency20Dispenser.setNextChain(currency10Dispenser);
    this.dispenseChain.setNextChain(currency20Dispenser);
  }

  @Override
  public void dispense(Currency currency) {
    System.out.println("Starting dispense.");
    dispenseChain.dispense(currency);
  }
}
