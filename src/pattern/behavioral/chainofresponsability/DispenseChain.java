package pattern.behavioral.chainofresponsability;

public interface DispenseChain extends Dispense {

  void setNextChain(DispenseChain nextChain);

}
