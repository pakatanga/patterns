package pattern.behavioral.mediator;

import java.util.LinkedList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator {

    private List<User> users;

    public ChatMediatorImpl() {
        this.users = new LinkedList<>();
    }

    @Override
    public void sendMessage(String message, User sender) {
        for (User user : this.users) {
            if (user != sender) {
                user.receive(message);
            }
        }
    }

    @Override
    public void addUser(User user) {
        this.users.add(user);
    }
}
