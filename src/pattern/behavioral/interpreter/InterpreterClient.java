package pattern.behavioral.interpreter;

public class InterpreterClient {

  private InterpreterContext interpreterContext;

  public InterpreterClient(InterpreterContext interpreterContext) {
    this.interpreterContext = interpreterContext;
  }

  public String interpret(String expression) {
    int i = extractIntFromExpresion(expression);

    if (expression.contains("Hexadecimal")) {
      return new IntToHexExpression(i).interpret(interpreterContext);
    }
    if (expression.contains("Binary")) {
      return new IntToBinaryExpression(i).interpret(interpreterContext);
    }
    return String.valueOf(i);
  }

  private int extractIntFromExpresion(String expression) {
    return Integer.parseInt(expression.substring(0, expression.indexOf(" ")));
  }

}
