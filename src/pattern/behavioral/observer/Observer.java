package pattern.behavioral.observer;

public interface Observer {

  String getName();

  void notifyMe(String message);

  void suscribe(Subject sub);

  void unsuscribe();

  void update(String message);

}
