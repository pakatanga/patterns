package pattern.behavioral.observer;

public class Stalker implements Observer {

  private String name;

  private Subject subject;

  public Stalker(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void notifyMe(String message) {
    System.out.println(message);
  }

  @Override
  public void suscribe(Subject subject) {
    this.subject = subject;
    subject.register(this);
  }

  @Override
  public void unsuscribe() {
    this.subject = null;
    subject.unregister(this);
  }

  @Override
  public void update(String message) {
    this.subject.updateTopic(this, message);
  }
}
