package pattern.behavioral.observer;

public interface Subject {

  void register(Observer obj);

  void unregister(Observer obj);

  void notifyObservers(String message);

  void updateTopic(Observer obj, String message);

}
