package pattern.behavioral.observer;

import java.util.LinkedList;
import java.util.List;

public class TopicOne implements Subject {

  private List<Observer> observers;

  public TopicOne() {
    this.observers = new LinkedList<>();
  }

  @Override
  public void register(Observer observer) {
    if (observer == null) {
      throw new NullPointerException("Null observer");
    }
    if (observers.contains(observer)) {
      throw new RuntimeException("Observer already registered");
    }
    notifyObservers("New observer added: " + observer.getName());
    observers.add(observer);
  }

  @Override
  public void unregister(Observer observer) {
    if (observer == null) {
      throw new NullPointerException("Null observer");
    }
    if (!observers.contains(observer)) {
      throw new RuntimeException("Observer not registered");
    }
    observers.remove(observer);
    notifyObservers("Observer removed: " + observer.getName());
  }

  @Override
  public void notifyObservers(String message) {
    for (Observer observer : observers) {
      observer.notifyMe(message);
    }
  }

  @Override
  public void updateTopic(Observer observer, String message) {
    notifyObservers("Observer " + observer.getName() + " says: " + message);
  }

}
