package pattern.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class Employees implements Cloneable {

    List<String> employeeList;

    public Employees() {
    }

    public Employees(List<String> employeeList) {
        this.employeeList = employeeList;
    }

    public List<String> getEmployeeList() {
        return employeeList;
    }

    public void add(String employee) {
        employeeList.add(employee);
    }

    public void remove(String employee) {
        employeeList.remove(employee);
    }

    public void clear() {
        employeeList.clear();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        List<String> temp = new ArrayList<>();
        for (String emp : getEmployeeList()) {
            temp.add(emp);
        }
        return new Employees(temp);
    }

}
