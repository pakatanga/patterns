package pattern.creational.abstractfactory.mixedapproach;

public interface AbstractComputerFactory {

    Computer createComputer();

}
