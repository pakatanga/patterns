package pattern.creational.abstractfactory.mixedapproach;

public class ComputerFactory {

    public static Computer createComputer(AbstractComputerFactory abstractComputerFactory) {
        return abstractComputerFactory.createComputer();
    }

}
