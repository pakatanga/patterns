package pattern.creational.abstractfactory.mixedapproach;

public abstract class AbstractComputerFactoryImpl implements AbstractComputerFactory {

    String cpu;
    String hdd;
    String ram;

    AbstractComputerFactoryImpl(String cpu, String hdd, String ram) {
        this.cpu = cpu;
        this.hdd = hdd;
        this.ram = ram;
    }

}
