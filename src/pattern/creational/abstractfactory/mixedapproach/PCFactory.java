package pattern.creational.abstractfactory.mixedapproach;

public class PCFactory extends AbstractComputerFactoryImpl {

    public PCFactory(String cpu, String hdd, String ram) {
        super(cpu, hdd, ram);
    }

    @Override
    public Computer createComputer() {
        return new PC(cpu, hdd, ram);
    }
}
