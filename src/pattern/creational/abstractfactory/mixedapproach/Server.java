package pattern.creational.abstractfactory.mixedapproach;

public class Server extends AbstractComputerImpl {

  Server(String cpu, String hdd, String ram) {
    super(cpu, hdd, ram);
  }

  @Override
  public String getCPU() {
    return cpu;
  }

  @Override
  public String getHDD() {
    return hdd;
  }

  @Override
  public String getRAM() {
    return ram;
  }
}
