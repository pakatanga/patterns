package pattern.creational.abstractfactory.mixedapproach;


public class ServerFactory extends AbstractComputerFactoryImpl {

    public ServerFactory(String cpu, String hdd, String ram) {
        super(cpu, hdd, ram);
    }

    @Override
    public Computer createComputer() {
        return new Server(cpu, hdd, ram);
    }
}
