package pattern.creational.abstractfactory.mixedapproach;

public abstract class AbstractComputerImpl implements Computer {

  protected String cpu;
  protected String hdd;
  protected String ram;

  public AbstractComputerImpl(String cpu, String hdd, String ram) {
    this.cpu = cpu;
    this.hdd = hdd;
    this.ram = ram;
  }

  @Override
  public abstract String getCPU();

  @Override
  public abstract String getHDD();

  @Override
  public abstract String getRAM();

}
