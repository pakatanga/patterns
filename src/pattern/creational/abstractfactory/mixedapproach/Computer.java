package pattern.creational.abstractfactory.mixedapproach;

public interface Computer {

  String getCPU();

  String getHDD();

  String getRAM();

}
