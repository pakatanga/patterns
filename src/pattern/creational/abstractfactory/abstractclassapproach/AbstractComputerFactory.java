package pattern.creational.abstractfactory.abstractclassapproach;

public interface AbstractComputerFactory {

    Computer createComputer();

}
