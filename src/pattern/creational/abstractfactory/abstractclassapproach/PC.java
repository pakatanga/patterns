package pattern.creational.abstractfactory.abstractclassapproach;

public class PC extends Computer {

  PC(String cpu, String hdd, String ram) {
    super(cpu, hdd, ram);
  }

  @Override
  public String getCPU() {
    return cpu;

  }

  @Override
  public String getHDD() {
    return hdd;
  }

  @Override
  public String getRAM() {
    return ram;
  }

}
