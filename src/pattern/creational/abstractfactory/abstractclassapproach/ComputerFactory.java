package pattern.creational.abstractfactory.abstractclassapproach;

public class ComputerFactory {

    public static Computer createComputer(AbstractComputerFactory abstractComputerFactory) {
        return abstractComputerFactory.createComputer();
    }
}
