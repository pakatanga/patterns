package pattern.creational.abstractfactory.abstractclassapproach;

public class PCFactory implements AbstractComputerFactory {

    private String cpu;
    private String hdd;
    private String ram;

    public PCFactory(String cpu, String hdd, String ram) {
        this.cpu = cpu;
        this.hdd = hdd;
        this.ram = ram;
    }

    @Override
    public Computer createComputer() {
        return new PC(cpu, hdd, ram);
    }
}
