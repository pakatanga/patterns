package pattern.creational.abstractfactory.abstractclassapproach;

public abstract class Computer {

  protected String cpu;
  protected String hdd;
  protected String ram;

  public Computer(String cpu, String hdd, String ram) {
    this.cpu = cpu;
    this.hdd = hdd;
    this.ram = ram;
  }

  public abstract String getCPU();

  public abstract String getHDD();

  public abstract String getRAM();

}
