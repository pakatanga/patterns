package pattern.creational.abstractfactory.interfaceapproach;

public class ComputerFactory {

    public static Computer createComputer(AbstractComputerFactory abstractComputerFactory) {
        return abstractComputerFactory.createComputer();
    }
}
