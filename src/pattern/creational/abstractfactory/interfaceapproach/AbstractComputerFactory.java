package pattern.creational.abstractfactory.interfaceapproach;

public interface AbstractComputerFactory {

    Computer createComputer();

}
