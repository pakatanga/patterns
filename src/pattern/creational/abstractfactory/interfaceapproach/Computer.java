package pattern.creational.abstractfactory.interfaceapproach;

public interface Computer {

  String getCPU();

  String getHDD();

  String getRAM();

}
