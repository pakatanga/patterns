package pattern.creational.factory.abstractclassapproach;

import pattern.creational.factory.ComputerType;

public class ComputerFactory {

  private ComputerFactory() {}

  public static Computer createComputer(ComputerType computerType, String cpu, String hdd, String ram) {
    switch (computerType) {
      case PC:
        return new PC(cpu, hdd, ram);
      case SERVER:
        return new Server(cpu, hdd, ram);
      default:
        return null;
    }
  }

}
