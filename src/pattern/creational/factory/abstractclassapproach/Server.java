package pattern.creational.factory.abstractclassapproach;

public class Server extends Computer {

  Server(String cpu, String hdd, String ram) {
    super(cpu, hdd, ram);
  }

  @Override
  public String getCPU() {
    return cpu;
  }

  @Override
  public String getHDD() {
    return hdd;
  }

  @Override
  public String getRAM() {
    return ram;
  }
}
