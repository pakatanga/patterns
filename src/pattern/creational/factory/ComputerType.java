package pattern.creational.factory;

public enum ComputerType {
  PC, SERVER
}
