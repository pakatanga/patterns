package pattern.creational.factory.interfaceapproach;

public interface Computer {

  String getCPU();

  String getHDD();

  String getRAM();

}
