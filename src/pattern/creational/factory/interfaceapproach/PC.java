package pattern.creational.factory.interfaceapproach;

public class PC implements Computer {

  private String cpu;
  private String hdd;
  private String ram;

  PC(String cpu, String hdd, String ram) {
    this.cpu = cpu;
    this.hdd = hdd;
    this.ram = ram;
  }

  @Override
  public String getCPU() {
    return cpu;
  }

  @Override
  public String getHDD() {
    return hdd;
  }

  @Override
  public String getRAM() {
    return ram;
  }
}
