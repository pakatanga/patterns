package pattern.creational.builder;

public class Computer {

    public static class Builder {

        private String macAddress;
        private String cpu;
        private String hdd;
        private String ram;

        public Builder(String macAddress) {
            this.macAddress = macAddress;
        }

        public Builder setCpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        public Builder setHdd(String hdd) {
            this.hdd = hdd;
            return this;
        }

        public Builder setRam(String ram) {
            this.ram = ram;
            return this;
        }

        public Computer build() {

            Computer computer = new Computer();
            computer.macAddress = this.macAddress;
            computer.cpu = this.cpu;
            computer.hdd = this.hdd;
            computer.ram = this.ram;
            return computer;
        }
    }

    private String macAddress;
    private String cpu;
    private String hdd;
    private String ram;

    private Computer() {}

}
