package pattern.creational.singleton;

public class ThreadSafeDoubleLockSingleton {

  private static ThreadSafeDoubleLockSingleton instance;

  private ThreadSafeDoubleLockSingleton() {}

  public static ThreadSafeDoubleLockSingleton getInstance() {
    if (instance == null) {
      synchronized (ThreadSafeDoubleLockSingleton.class) {
        instance = new ThreadSafeDoubleLockSingleton();
      }
    }
    return instance;
  }

}
