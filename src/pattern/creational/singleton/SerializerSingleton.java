package pattern.creational.singleton;

import java.io.Serializable;

public class SerializerSingleton implements Serializable {

  private SerializerSingleton() {}

  public static SerializerSingleton getInstance() {
    return SingletonHelper.INSTANCE;
  }

  protected Object readResolve() {
    return getInstance();
  }

  private static class SingletonHelper {

    private static final SerializerSingleton INSTANCE = new SerializerSingleton();

  }

}
