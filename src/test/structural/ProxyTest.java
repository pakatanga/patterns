package test.structural;

import pattern.structural.proxy.CommandExecutor;
import pattern.structural.proxy.CommandExecutorProxy;

public class ProxyTest {

    public void proxyTest(){
        CommandExecutor cmd = new CommandExecutorProxy("PAACOOO", "meeeck");
        try {
            cmd.runCommand("hello");
            cmd.runCommand("rm aa");
        } catch (Exception ex) {
            System.out.println("EEERROOOOOOOOOOR");
        }
    }

}
