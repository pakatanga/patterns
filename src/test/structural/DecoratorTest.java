package test.structural;

import pattern.structural.decorator.BasicCar;
import pattern.structural.decorator.Car;
import pattern.structural.decorator.LuxuryCar;
import pattern.structural.decorator.SportCar;

public class DecoratorTest {

    public void decoratorTest() {
        Car basicCar = new BasicCar();

        Car sportCar = new SportCar(basicCar);

        Car luxuryCar = new LuxuryCar(basicCar);

        Car sportLuxuryCar = new LuxuryCar(new SportCar( new BasicCar()));
    }

}
