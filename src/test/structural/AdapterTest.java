package test.structural;

import pattern.structural.adapter.SocketObjectAdapterImpl;

public class AdapterTest {

    public void adapterTest() {
        SocketObjectAdapterImpl socketObjectAdapter = new SocketObjectAdapterImpl();
        socketObjectAdapter.get120Volt();
        socketObjectAdapter.get12Volt();
        socketObjectAdapter.get3Volt();
    }

}
