package test.structural;

import pattern.structural.composite.Circle;
import pattern.structural.composite.Drawing;
import pattern.structural.composite.Triangle;

public class CompositeTest {

    public void compositeTest() {
        Drawing drawing = new Drawing();
        drawing.add(new Triangle());
        drawing.add(new Circle());
        drawing.add(new Drawing());

        drawing.draw("VEERDEEE");
    }

}
