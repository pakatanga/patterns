package test.structural;

import pattern.structural.bridge.*;

public class BridgeTest {

    public void bridgeTest() {
        Shape tri = new Triangle(new RedColor());
        tri.paint();

        Shape pent = new Pentagon(new GreenColor());
        pent.paint();
    }

}
