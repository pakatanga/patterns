package test.creational;

import pattern.creational.factory.ComputerType;
import pattern.creational.factory.abstractclassapproach.ComputerFactory;

public class FactoryTest {

    public void factory() {
        ComputerFactory.createComputer(ComputerType.PC, "", "", "");
        ComputerFactory.createComputer(ComputerType.SERVER, "", "", "");
    }

}
