package test.creational;

import pattern.creational.abstractfactory.mixedapproach.Computer;
import pattern.creational.abstractfactory.mixedapproach.ComputerFactory;
import pattern.creational.abstractfactory.mixedapproach.PCFactory;
import pattern.creational.abstractfactory.mixedapproach.ServerFactory;

public class AbstractFactoryTest {

    public void abstractFactoryTest() {
        PCFactory pcFactory = new PCFactory("", "", "");
        ServerFactory serverFactory = new ServerFactory("", "", "");
        Computer computer = ComputerFactory.createComputer(serverFactory);
        Computer computer2 = ComputerFactory.createComputer(pcFactory);
    }

}
