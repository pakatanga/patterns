package test.creational;

import pattern.creational.prototype.Employees;

import java.util.Arrays;
import java.util.List;

public class PrototypeTest {

    public void prototypeTest() throws CloneNotSupportedException{
        String[] employeesArray = {"Paco", "Tronco", "Mery"};

        Employees employees = new Employees(Arrays.asList(employeesArray));

        Employees clone = (Employees) employees.clone();
        clone.add("perico de los palotes");

        Employees clone2 = (Employees) employees.clone();
        clone2.remove("Paco");

        Employees clone3 = (Employees) employees.clone();
        clone3.clear();

    }

}
