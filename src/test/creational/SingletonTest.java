package test.creational;

import pattern.creational.singleton.*;

public class SingletonTest {

    public void eagerInitializedSingleton() {
        EagerInitializedSingleton instance = EagerInitializedSingleton.getInstance();
    }

    public void staticBlockSingleton() {
        StaticBlockSingleton instance = StaticBlockSingleton.getInstance();
    }

    public void lazyInitializedSingleton() {
        LazyInitializedSingleton instance = LazyInitializedSingleton.getInstance();
    }

    public void threadSafeSingleton() {
        ThreadSafeSingleton instance = ThreadSafeSingleton.getInstance();
    }

    public void threadSafeDoubleLockSingleton() {
        ThreadSafeDoubleLockSingleton instance = ThreadSafeDoubleLockSingleton.getInstance();
    }

    public void serializerSingleton() {
        SerializerSingleton instance = SerializerSingleton.getInstance();
    }

    public void billPughSingleton() {
        BillPughSingleton instance = BillPughSingleton.getInstance();
    }

}
