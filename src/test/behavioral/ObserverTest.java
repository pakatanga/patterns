package test.behavioral;

import pattern.behavioral.observer.Observer;
import pattern.behavioral.observer.Stalker;
import pattern.behavioral.observer.Subject;
import pattern.behavioral.observer.TopicOne;

public class ObserverTest {

  public void observerTest() {
    Subject subject = new TopicOne();

    Observer observer1 = new Stalker("Paco");
    Observer observer2 = new Stalker("Mery");
    Observer observer3 = new Stalker("Tronco");

    observer1.suscribe(subject);
    observer2.suscribe(subject);
    observer3.suscribe(subject);

    observer3.update("GUAU!");
    observer2.update("HOLAAA PERROOO");
    observer2.unsuscribe();
    observer1.update("XEE QUE PLOM");

  }

}
