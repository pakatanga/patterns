package test.behavioral;

import pattern.behavioral.command.*;

public class CommantTest {

  public static FileSystemReceiver getUnderlyingFileSystem() {
    String osName = System.getProperty("os.name");
    System.out.println("Underlying OS is:" + osName);
    if (osName.contains("Windows")) {
      return new WindowsFileSystemReceiver();
    } else {
      return new UnixFileSystemReceiver();
    }
  }

  public void commandTest() {
    FileSystemReceiver fs = getUnderlyingFileSystem();

    // creating command and associating with receiver
    OpenFileCommand openFileCommand = new OpenFileCommand(fs);

    // Creating invoker and associating with Command
    FileInvoker file = new FileInvoker(openFileCommand);

    // perform action on invoker object
    file.execute();

    WriteFileCommand writeFileCommand = new WriteFileCommand(fs);
    file = new FileInvoker(writeFileCommand);
    file.execute();

    CloseFileCommand closeFileCommand = new CloseFileCommand(fs);
    file = new FileInvoker(closeFileCommand);
    file.execute();
  }

}
