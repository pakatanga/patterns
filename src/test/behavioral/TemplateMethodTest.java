package test.behavioral;

import pattern.behavioral.templatemethod.GlassHouse;
import pattern.behavioral.templatemethod.HouseTemplate;
import pattern.behavioral.templatemethod.WoodenHouse;

public class TemplateMethodTest {

    public void templateMethodTest() {
        HouseTemplate houseType = new WoodenHouse();

        //using template method
        houseType.buildHouse();
        System.out.println("************");

        houseType = new GlassHouse();

        houseType.buildHouse();
    }

}
