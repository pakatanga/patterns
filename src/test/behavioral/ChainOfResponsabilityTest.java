package test.behavioral;

import pattern.behavioral.chainofresponsability.ATMDispenser;
import pattern.behavioral.chainofresponsability.Currency;

public class ChainOfResponsabilityTest {

  public void chainOfResponsabilityTest() {
    ATMDispenser atmDispenser = new ATMDispenser();
    atmDispenser.dispense(new Currency(562));
  }

}
