package test.behavioral;

import pattern.behavioral.mediator.ChatMediator;
import pattern.behavioral.mediator.ChatMediatorImpl;
import pattern.behavioral.mediator.User;
import pattern.behavioral.mediator.UserImpl;

public class mediatorTest {

    public void mediatorTest() {
        ChatMediator mediator = new ChatMediatorImpl();

        User user1 = new UserImpl(mediator, "Paco");
        User user2 = new UserImpl(mediator, "Mery");
        User user3 = new UserImpl(mediator, "Tronco");

        user3.send("GUAU");

    }

}
