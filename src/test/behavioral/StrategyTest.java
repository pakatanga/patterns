package test.behavioral;

import pattern.behavioral.strategy.CreditCardPayment;
import pattern.behavioral.strategy.Item;
import pattern.behavioral.strategy.PaypalPayment;
import pattern.behavioral.strategy.ShoppingCart;

public class StrategyTest {

  public void strategyTest() {
    ShoppingCart cart = new ShoppingCart();

    Item item1 = new Item("1234", 10);
    Item item2 = new Item("5678", 40);

    cart.addItem(item1);
    cart.addItem(item2);

    cart.pay(new PaypalPayment("myemail@example.com", "mypwd"));

    cart.pay(new CreditCardPayment("Pankaj Kumar", "1234567890123456", "786"));
  }

}
