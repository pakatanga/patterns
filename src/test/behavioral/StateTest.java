package test.behavioral;

import pattern.behavioral.action.StartedTVState;
import pattern.behavioral.action.State;
import pattern.behavioral.action.StoppedTVAction;
import pattern.behavioral.action.TVContext;

public class StateTest {

  public void stateTest() {

    TVContext context = new TVContext();
    State tvStartState = new StartedTVState();
    State tvStopState = new StoppedTVAction();

    context.setTvState(tvStartState);
    context.doAction();

    context.setTvState(tvStopState);
    context.doAction();

  }

}
